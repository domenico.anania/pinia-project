import { defineStore } from 'pinia'

export const useAuthStore = defineStore('auth', {
    state: () => {
        return {
            isAuthenticated: false,
            user: {/*
                name: 'Domenico',
                email: 'dome@nico.com',*/
            },
        }

    },
    getters:{
        fullName: (state) => `${state.user.first_name} ${state.user.last_name}`
    },
    actions:{
        logOut(){
            //così facendo imostiamo a false l'autenticazione la scritta 'scompare', ma se andiamo su vue devtools notiamo che l'oggetto user è ancora valido
            //this.isAuthenticated = false;
      
            //this.$state = {isAuthenticated: false, user: {}}
      
            //NB: rivedere differenze tra $state e $patch
            this.$patch((state) => {
              (state.isAuthenticated = false), (state.user = {});
            });
          },
          async logIn(){
            //$reset porta ai dati originali impostati nello store
           // this.$reset();
           //nelle situazioni reali andiamo al login andiamo a fare una richiesta a un api

           const res = await fetch('https://reqres.in/api/users/2')
           const {data} = await res.json() //{data} destrutturazione, la promise invia i dati e leggo solo l'oggetto data
           //console.log(data)
           this.user = data
           //console.log(this.user)
           this.isAuthenticated = true
          }
    }
})