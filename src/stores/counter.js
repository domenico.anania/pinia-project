import { defineStore } from 'pinia'
import { useAuthStore } from './auth'

export const useCounterStore = defineStore('counter', {
    //in state inseriamo i nostri data
    state: () => {
        return {
            count: 0,
        }
    },

    //le getters sono come le computed properties di un componente
    //con le getters andiamo a prendere e modificare i valori che abbiamo in state
    getters: {
        countDigitLength: (state) => state.count.toString().length
    },
    actions: {
        increment() {           
            //if(!authStore.isAuthenticated) return
            if (this.isAuthenticated()) {
                this.count++
            }

        },

        decrement() {
            if (this.isAuthenticated()) {
                this.count--
            }

        },
        isAuthenticated(){
            const authStore = useAuthStore();
            return authStore.isAuthenticated
        }
    }

})